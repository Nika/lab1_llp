#include "../include/db/row.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

void free_header_row(struct header_row* old_row) {
	free(old_row);
}

void free_row(struct row* old_row) {
	free(old_row->values);
	free(old_row);
}

struct row* create_row(struct header_row* header) {
	struct row* new_row = (struct row*) malloc(sizeof(struct row));
	new_row->header_row = header;
	new_row->values = (void*) malloc(header->size);
	return new_row;
}

uint32_t get_row_offset(struct row* cur_row, uint32_t cur_row_number, bool is_first_page) {
	if (is_first_page) {
		return sizeof(struct page_header) + cur_row->header_row->size_of_header + cur_row->header_row->size * cur_row_number;
	} else {
		return sizeof(struct page_header) + cur_row->header_row->size * cur_row_number;
	}
}

void* get_value_from_row(struct row* row, char* column_name) {
	struct header_value* h_v = get_header_value_of_col(row->header_row, column_name);
	if (h_v == NULL) {
		return NULL;
	}
	return (void*)((char*)row->values + h_v->offset_of_value);
}

void delete_row_from_page(struct page_header* cur_page, struct row* cur_row, uint32_t cur_row_number, bool is_first_page) {
	if (cur_row_number < count_rows_in_page(cur_page, cur_row, is_first_page) - 1) {
		memcpy((char*)cur_page + get_row_offset(cur_row, cur_row_number, is_first_page), (char*)cur_page + get_row_offset(cur_row, cur_row_number + 1, is_first_page), 
			(count_rows_in_page(cur_page, cur_row, is_first_page) - cur_row_number - 1)*cur_row->header_row->size);
	}
	cur_page->current_place = cur_page->current_place - cur_row->header_row->size;
	cur_page->free_size = cur_page->free_size + cur_row->header_row->size;
}

void update_row_in_page(struct page_header* cur_page, struct row* cur_row, uint32_t cur_row_number, bool is_first_page, void* new_value,  char* column_name) {
	void* value = get_value_from_row(cur_row, column_name); 
	struct header_value* h_v = get_header_value_of_col(cur_row->header_row, column_name);
	memcpy((char*)cur_page + get_row_offset(cur_row, cur_row_number, is_first_page) + h_v->offset_of_value, new_value, h_v->size);
}


uint32_t count_rows_in_page(struct page_header* page, struct row* row, bool is_first_page){
	if (!page) {
		print_error("Incorrect page pointer", "");
		return -1;
	}
	if (is_first_page) {
		return (page->current_place - sizeof(struct page_header) - row->header_row->size_of_header)/row->header_row->size;
	} else {
		return (page->current_place - sizeof(struct page_header))/row->header_row->size;
	}
}

struct header_value* get_header_value_of_col(struct header_row* header, char* col_name) {
	int is_found = 0;
	struct header_value* cur_val = header->first_val;
	while (!is_found && cur_val) {
		if (strcmp(cur_val->name, col_name) == 0) {
			is_found = 1;
		} else {
			cur_val = cur_val->next_header_value;
		}
	}
	if (is_found == 0) {
		return NULL;
	}
	return cur_val;
}

void insert_bool_value(struct row* row, void* value, uint32_t offset) {
    memcpy((char*)row->values + offset, (bool*)value, sizeof(bool));
}

void insert_double_value(struct row* row, void* value, uint32_t offset) {
	memcpy((char*)row->values + offset, (double*)value, sizeof(double));
}

void insert_string_value(struct row* row, void* value, uint32_t offset) {
    strcpy(((char*)row->values + offset), (char*)value);
}

void insert_int32_t_value(struct row* row, void* value, uint32_t offset) {
    memcpy((char*)row->values + offset, (int32_t*)value, sizeof(uint32_t));
}

int insert_value_in_row(struct row* row, char* col_name, void* value) {
	struct header_row* header = row->header_row;
	struct header_value* cur_header_value = get_header_value_of_col(header, col_name);
	if (!cur_header_value) {
		print_info("Can't find column %s for insreting new value", col_name);
		return 0;
	}
	uint32_t offset = cur_header_value->offset_of_value;
	enum value_type value_type = cur_header_value->type;
	switch (value_type) {
		case TYPE_INT32:
			insert_int32_t_value(row, value, offset);
			break;
		case TYPE_STRING:
			insert_string_value(row, value, offset);
			break;
		case TYPE_FLOAT:
			insert_double_value(row, value, offset);
			break;
		case TYPE_BOOL:
			insert_bool_value(row, value, offset);
	}
	return 1;
}

uint32_t size_of_string_value(enum value_type type, uint32_t len) {
	if (type != TYPE_STRING) {
		return -1;
	}
	return sizeof(char) * len;

}

uint32_t size_of_simple_value(enum value_type type) {
	switch (type) {
		case TYPE_INT32:
			return sizeof(int32_t);
		case TYPE_STRING:
			return -1;
		case TYPE_FLOAT:
			return sizeof(double);
		case TYPE_BOOL:
			return sizeof(bool);
	}
}

bool compare_int_value(void* old_value, void* new_value) {
    if (*((int32_t*) old_value) == *((int32_t*) new_value)) return true;
    return false;
}

bool compare_bool_value(void* old_value, void* new_value) {
    if (*((bool*) old_value) == *((bool*) new_value)) return true;
    return false;
}

bool compare_float_value(void* old_value, void* new_value) {
    if (*((double*) old_value) == *((double*) new_value)) return true;
    return false;
}

bool compare_string_value(void* old_value, void* new_value) {
    if (strcmp((char*) old_value, (char*) new_value) == 0) return true;
    return false;
}



bool compare_value_with_field_in_row(struct row* row, void* value, char* column_name){
	void* cur_value = get_value_from_row(row, column_name);
	if (cur_value == NULL) {
		return false;
	}
	struct header_value* h_v = get_header_value_of_col(row->header_row, column_name);
	switch (h_v->type) {
		case TYPE_INT32:
			return compare_int_value(cur_value, value);
		case TYPE_STRING:
			return compare_string_value(cur_value, value);
		case TYPE_FLOAT:
			return compare_float_value(cur_value, value);
		case TYPE_BOOL:
			return compare_bool_value(cur_value, value);
	}

}

bool compare_fields_in_rows(struct row* left_row, struct row* right_row, char* left_header_name, char* right_header_name) {
	void* left_value = get_value_from_row(left_row, left_header_name);
	if (left_value == NULL) {
		return false;
	}
	return compare_value_with_field_in_row(right_row, left_value, right_header_name);
}

struct header_row* add_column_to_table_header(struct header_row* header, struct header_value* header_value, enum value_type type, char* name) {
	header_value->type = type;
	strcpy(header_value->name, name);
	struct header_value* prev_header_value = header->first_val;
	uint32_t offset = 0;
	if (!prev_header_value) {
		header->first_val = header_value;
		header->size = 0;
		header->size_of_header = 0;
	} else {
	 	while (prev_header_value->has_next == true) {
	 	  	prev_header_value = prev_header_value->next_header_value;
	 	}
	 	offset += prev_header_value->offset_of_value + prev_header_value->size;
		prev_header_value->next_header_value = header_value;
	 	prev_header_value->has_next = true;
	}
	header->size += header_value->size;
	header->size_of_header+=sizeof(struct header_value); 
	header_value->offset_of_value = offset;
	header_value->has_next = false;

	return header;
}