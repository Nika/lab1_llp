#include "../include/db/table.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

void delete_table(struct table* old_table) {
	add_to_db_stack(old_table->db, old_table->start_page_number);
	uint32_t next_page_number = clear_page(old_table->start_page_number);
	while (next_page_number!=0 && next_page_number!=-1) {
		add_to_db_stack(old_table->db, next_page_number);
		next_page_number = clear_page(next_page_number);
	}
	delete_table_from_meta_pages(old_table->schema->name, old_table->db);
	print_info("Table %s deleted", old_table->schema->name);
	free_table(old_table);
}

void free_table(struct table* old_table) {
	free_header_row(old_table->schema->header);
	free(old_table->schema);
	free(old_table);
}

struct row* create_row_in_table(struct table* table) {
	return create_row(table->schema->header);
}


void add_to_table_stack(struct table* table, uint64_t page_number) {
	struct free_page_stack* new_stack_el = (struct free_page_stack*) malloc(sizeof(struct free_page_stack));
	new_stack_el->prev_el = table->stack;
	new_stack_el->page_number = page_number;
	table->stack = new_stack_el;
}

uint64_t take_from_table_stack(struct table* table) {
	if (table->stack == NULL) {
		return -1;
	}
	struct free_page_stack* cur_el = table->stack;
	table->stack = table->stack->prev_el;
	uint64_t page_number = cur_el->page_number;
	free(cur_el);
	return page_number;
}

enum write_status insert_row_in_table(struct row* row, struct table *table) {
	uint32_t curr_page_number = table->end_page_number;
	struct page_header* curr_page = get_page(curr_page_number);
	if (!curr_page) {
		print_error("Cant't get page number %d", curr_page_number);
		return WRITE_ERROR;
	}
	struct header_row* header = row->header_row;
	enum write_status ws = write_smth(curr_page, row->values, header->size); 
	while (ws == WRITE_ERROR) {
		if (table->stack == NULL) {
			curr_page_number = get_last_page_number() + 1;
			table->end_page_number = curr_page_number;
			curr_page->next_page_number = curr_page_number;
			unmap_smth(curr_page);
			curr_page = init_page(curr_page_number, true);
		} else {
			curr_page_number = take_from_table_stack(table);
			unmap_smth(curr_page);
			curr_page = get_page(curr_page_number);
		}
		if (!curr_page) {
			return WRITE_ERROR;
		}
	 	ws = write_smth(curr_page, row->values, header->size); 
	}
	table->rows_count += 1;
	rewrite_table_info(table);
	//print_info("Insert row in table %s", table->schema->name);
	unmap_smth(curr_page);
	return WRITE_OK;
}

struct table* create_table_from_schema(struct schema* schema, struct database* db) {
	struct table* new_table = (struct table*) malloc(sizeof(struct table));
	new_table->schema = schema;
	new_table->rows_count = 0;
	new_table->stack = NULL;
	enum add_status as = add_table_to_db(new_table, db);
	if (as == ADD_ERROR){
		print_error("Can't add table %s to db", schema->name);
		free_table(new_table);
		return NULL;
	}
	if (as == ADD_ERROR_DUPLICATE) {
		print_error("Dublicate name of table %s", schema->name);
		free_table(new_table);
		return NULL;
	}
	return new_table;
}

struct schema* create_new_schema(char* t_name) {
	print_info("Create new schema %s", t_name);
	struct schema* new_schema = (struct schema*) malloc(sizeof(struct schema));
	strcpy(new_schema->name, t_name);
	struct header_row* header = (struct header_row*) malloc(sizeof(struct header_row));
	new_schema->header = header;
	new_schema->header->first_val = NULL;
	new_schema->header->size = 0;
	new_schema->header->size_of_header = 0;
	return new_schema;
}

struct table* get_table_from_file (char* t_name, struct database* db) {
	struct table* table = get_table_from_db(t_name, db);
	table->stack = NULL;
	return table; 
}

struct schema* add_simple_column_to_table_header(struct schema* schema, enum value_type type, char* name) {
	struct header_value* header_value = (struct header_value*) malloc(sizeof(struct header_value));
	header_value->size = size_of_simple_value(type);
	schema->header = add_column_to_table_header(schema->header, header_value, type, name);
	return schema;
}

struct schema* add_string_column_to_table_header(struct schema* schema, enum value_type type, char* name, uint32_t len) {
	struct header_value* header_value = (struct header_value*) malloc(sizeof(struct header_value));
	header_value->size = size_of_string_value(type, len);
	schema->header = add_column_to_table_header(schema->header, header_value, type, name);
	return schema;
}

