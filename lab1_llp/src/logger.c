#include "../include/logger.h"

void print_info(const char *format, void* arg) {
    printf("\033[0;34m[info]"); 
    printf(format, arg);
    printf("\033[0m\n"); 
}

void print_error(const char *format, void* arg) {
    printf("\033[0;31m[error]"); 
    printf(format, arg);
    printf("\033[0m\n"); 
}