#include "../include/db/table.h"
#include "../include/file/file.h"
#include "../include/query/query.h"
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void test_create_and_fill(){

	printf("------------%s--------------\n", "test1 - create tables and fill");

	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);

	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	struct schema* new_schema1 = create_new_schema("Days_of_week");
	new_schema1 = add_simple_column_to_table_header(new_schema1, TYPE_BOOL, "is_weekend");
	new_schema1 = add_simple_column_to_table_header(new_schema1, TYPE_INT32, "id");
	new_schema1 = add_string_column_to_table_header(new_schema1, TYPE_STRING, "name", 15);
	struct table* new_table1 = create_table_from_schema(new_schema1, db);

	struct row* new_row = create_row_in_table(new_table2);

	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[4] = {false, true, false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	for (int i = 0; i < 50; i++) {
		insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
		insert_value_in_row(new_row, "approved", &is_approved[i % 4]);
		insert_value_in_row(new_row, "full_name", full_name[i % 3]);
		insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
		insert_value_in_row(new_row, "id", &i);
		enum write_status ws = insert_row_in_table(new_row, new_table2);
	}

	new_row = create_row_in_table(new_table1);
	bool is_weekend[7] = {false, false, false, false, false, true, true};
	char* name[7] = {"monday", "tuesday", "wednsday", "thursday", "friday", "saturday", "sunday"};
	for (int i = 0; i < 7; i++) {
		insert_value_in_row(new_row, "is_weekend", &is_weekend[i]);
		insert_value_in_row(new_row, "name", name[i]);
		insert_value_in_row(new_row, "id", &numbers[i]);
		enum write_status ws = insert_row_in_table(new_row, new_table1);
	}


	free_table(new_table1);
	free_table(new_table2);
	free_row(new_row);
	free_db(db);

	printf("------------%s--------------\n", "test1 is completed");
}

void test_read_and_select(){
	printf("------------%s--------------\n", "test2 - read tables and select");
	struct database* db = get_db_from_file("db");
	
	struct table* new_table1 = get_table_from_file("Days_of_week", db);
	struct table* new_table2 = get_table_from_file("Meetings", db);

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[4] = {false, true, false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	for (int i = 50; i < 100; i++) {
		insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
		insert_value_in_row(new_row, "approved", &is_approved[i % 4]);
		insert_value_in_row(new_row, "full_name", full_name[i % 3]);
		insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
		insert_value_in_row(new_row, "id", &i);
		enum write_status ws = insert_row_in_table(new_row, new_table2);
	}

	printf("\n%s\n", "select * from Days_of_week where is_weekend = false");
	bool first_val = false;
	struct query* query = get_select_query(new_table1, "is_weekend", &first_val);
	execute_query(query);

	printf("\n%s\n", "select * from Meetings where full_name = 'Troynikova Veronika'");
	char* second_val = "Troynikova Veronika";
	query = get_select_query(new_table2, "full_name", second_val);
	execute_query(query);

	printf("\n%s\n", "select * from Meetings where id_day_of_week = 1");
	int third_val = 1;
	query = get_select_query(new_table2, "id_day_of_week", &third_val);
	execute_query(query);

	delete_table(new_table1);
	delete_table(new_table2);

	//free_row(new_row);
	free_db(db);
	free_query(query);

	printf("------------%s--------------\n", "test2 is completed");
}

void test_insert(){
	printf("------------%s--------------\n", "test3 -create table and insert big amount of values");
	
	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);
	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[4] = {false, true, false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};

	for (int j = 0; j < 100; j++) {
		clock_t begin = clock();
		for (int i = 0; i < 10; i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &is_approved[i % 4]);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
			insert_value_in_row(new_row, "id", &i);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
		}
        clock_t end = clock();
    	printf("Insert %d rows by %f seconds\n", 10 * (j+1), (double)(end - begin) / CLOCKS_PER_SEC);
	}


	delete_table(new_table2);
	free_row(new_row);
	free_db(db);

	printf("------------%s--------------\n", "test3 is completed");
}


void test_select(){
	printf("------------%s--------------\n", "test4 - read table and select big amount of values");
	struct database* db = get_db_from_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);
	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	bool selected_value = false;
	struct query* query = get_select_query(new_table2, "approved", &(selected_value));

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[2] = {false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 100; i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &is_approved[i%2]);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
			insert_value_in_row(new_row, "id", &i);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
		}
		clock_t begin = clock();
		execute_query(query);
        clock_t end = clock();
        printf("Select from %d rows by %f seconds\n", 100*(j+1), (double)(end - begin) / CLOCKS_PER_SEC);
	}

	free_table(new_table2);
	free_row(new_row);
	free_query(query);
	free_db(db);

	printf("------------%s--------------\n", "test4 is completed");
}

void test_update(){
	printf("------------%s--------------\n", "test5 - create table and update big amount of values");
	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);
	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	bool selected_value = false;
	struct query* query = get_update_query(new_table2, "approved", &(selected_value), &(selected_value));

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[2] = {false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 100; i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &is_approved[i%2]);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
			insert_value_in_row(new_row, "id", &i);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
		}
		clock_t begin = clock();
		execute_query(query);
        clock_t end = clock();
        printf("Update from %d rows by %f seconds\n", 100*(j+1), (double)(end - begin) / CLOCKS_PER_SEC);
	}

	free_table(new_table2);
	free_row(new_row);
	free_query(query);
	free_db(db);

	printf("------------%s--------------\n", "test5 is completed");
}

void test_delete(){
	printf("------------%s--------------\n", "test6 - create table and delete big amount of values");
	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);
	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	bool selected_value = false;
	struct query* query = get_delete_query(new_table2, "approved", &(selected_value));

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[2] = {false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 10; i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &is_approved[i%2]);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
			insert_value_in_row(new_row, "id", &i);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
		}
		clock_t begin = clock();
		execute_query(query);
        clock_t end = clock();
        printf("Delete from %d rows by %f seconds\n", 10*(j+1), (double)(end - begin) / CLOCKS_PER_SEC);
		for (int i = 0; i < 5*(j + 1); i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &selected_value);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
			insert_value_in_row(new_row, "id", &i);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
		}
	}

	free_table(new_table2);
	free_row(new_row);
	free_query(query);
	free_db(db);

	printf("------------%s--------------\n", "test6 is completed");
}

void test_size_of_file(){
	printf("------------%s--------------\n", "test7 -create table, insert values and check file size");
	
	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);
	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_approved[4] = {false, true, false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};

	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < 100; i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &is_approved[i % 4]);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
			insert_value_in_row(new_row, "id", &i);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
		}
    	printf("Insert %d rows and file size is %ld bytes\n", 100 * (j + 1), get_file_size());
	}


	delete_table(new_table2);
	free_row(new_row);
	free_db(db);

	printf("------------%s--------------\n", "test7 is completed");
}


void test_join(){

	printf("------------%s--------------\n", "test8 - create tables, fill and execute join query");

	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);

	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	struct schema* new_schema1 = create_new_schema("Days_of_week");
	new_schema1 = add_simple_column_to_table_header(new_schema1, TYPE_BOOL, "is_weekend");
	new_schema1 = add_simple_column_to_table_header(new_schema1, TYPE_INT32, "id");
	new_schema1 = add_string_column_to_table_header(new_schema1, TYPE_STRING, "name", 15);
	struct table* new_table1 = create_table_from_schema(new_schema1, db);

	struct row* new_row = create_row_in_table(new_table1);
	int numbers[7] = {1, 2, 3, 4, 5, 6, 7};
	bool is_weekend[7] = {false, false, false, false, false, true, true};
	char* name[7] = {"monday", "tuesday", "wednsday", "thursday", "friday", "saturday", "sunday"};
	for (int i = 0; i < 7; i++) {
		insert_value_in_row(new_row, "is_weekend", &is_weekend[i]);
		insert_value_in_row(new_row, "name", name[i]);
		insert_value_in_row(new_row, "id", &numbers[i]);
		enum write_status ws = insert_row_in_table(new_row, new_table1);
	}

	new_row = create_row_in_table(new_table2);

	bool is_approved[4] = {false, true, false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	for (int i = 0; i < 10; i++) {
		insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
		insert_value_in_row(new_row, "approved", &is_approved[i % 4]);
		insert_value_in_row(new_row, "full_name", full_name[i % 3]);
		insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 7]);
		insert_value_in_row(new_row, "id", &i);
		enum write_status ws = insert_row_in_table(new_row, new_table2);
	}

	struct join_query* query = get_join_query(new_table1, new_table2, "id", "id_day_of_week");
	execute_join_query(query);

	free_table(new_table1);
	free_table(new_table2);
	free_join_query(query);
	free_row(new_row);
	free_db(db);

	printf("------------%s--------------\n", "test8 is completed");
}

void test_insert_delete_time(){
	printf("------------%s--------------\n", "test9 - create table and in cycle fill 500 rows and delete 400 rows");
	struct database* db = create_db_in_file("db");
	
	struct schema* new_schema2 = create_new_schema("Meetings");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_BOOL, "approved");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_FLOAT, "duration_in_hours");
	new_schema2 = add_simple_column_to_table_header(new_schema2, TYPE_INT32, "id_day_of_week");
	new_schema2 = add_string_column_to_table_header(new_schema2, TYPE_STRING, "full_name", 30);
	struct table* new_table2 = create_table_from_schema(new_schema2, db);

	struct query* query = NULL;

	struct row* new_row = create_row_in_table(new_table2);
	int numbers[7] = {1, 2, 3, 4, 5};
	bool is_approved[2] = {false, true};
	char* full_name[3] = {"Troynikova Veronika", "Menkov Danil", "Lushnikova Nasty"};
	double durations[5] = {1.0, 2.0, 3.5, 2.5, 1.2};
	clock_t begin = 0;
	clock_t end = 0;
	for (int j = 0; j < 20; j++) {
		begin = clock();
		for (int i = 1; i <= 500*(j + 1); i++){
			insert_value_in_row(new_row, "duration_in_hours", &durations[i % 5]);
			insert_value_in_row(new_row, "approved", &is_approved[i%2]);
			insert_value_in_row(new_row, "full_name", full_name[i % 3]);
			insert_value_in_row(new_row, "id_day_of_week", &numbers[i % 5]);
			int id = j*500 + i;
			insert_value_in_row(new_row, "id", &id);
			enum write_status ws = insert_row_in_table(new_row, new_table2);
			if (i % 100 == 0) {
				end = clock();
				//printf("Insert %d rows by %f seconds\n", 100, (double)(end - begin) / CLOCKS_PER_SEC);
				begin = clock();
			}
		}
		begin = clock();
		for (int i = 1; i <= 300 * (j + 1); i++) {
			int id = j*500 + i;
			query = get_delete_query(new_table2, "id", &(id));
			execute_query(query);
			if (i % 100 == 0) {
				end = clock();
				printf("Delete %d rows by %f seconds\n", 100, (double)(end - begin) / CLOCKS_PER_SEC);
				begin = clock();
			}
		}
		
	}
	free_table(new_table2);
	free_row(new_row);
	free_query(query);
	free_db(db);

	printf("------------%s--------------\n", "test9 is completed");
}

int main() {
	test_create_and_fill();
	test_read_and_select();
	test_insert();
	test_select();
	test_update();
	test_delete();
	test_size_of_file();
	test_join();
	test_insert_delete_time();
	return 0;
}