#include "../include/query/output_util.h"
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

void print_float(void* values, uint64_t offset) {
    printf("%f | ", *((double*)((char*)values + offset)));
}

void print_int(void* values, uint64_t offset) {
    printf("%d | ", *((int*)((char*)values + offset)));
}

void print_string(void* values, uint64_t offset) {
    printf("%s | ", (char*)values + offset);
}

void print_bool(void* values, uint64_t offset) {
    if (*(bool*)((char*)values + offset) == true) {
        printf("%s | ", "true");
    } else {
        printf("%s | ", "false");
    }
    
}


void print_row (struct row* row) {
    struct header_value* h_v = row->header_row->first_val;
    while (h_v) {
        switch (h_v->type) {
            case TYPE_INT32:
                print_int(row->values, h_v->offset_of_value);
                break;
            case TYPE_STRING:
                print_string(row->values, h_v->offset_of_value);
                break; 
            case TYPE_FLOAT:
                print_float(row->values, h_v->offset_of_value);
                break; 
            case TYPE_BOOL:
                print_bool(row->values, h_v->offset_of_value);
                break;
        }
        h_v = h_v->next_header_value;
    }
    printf("\n");
}

void print_connected_rows(struct row* left_row, struct row* right_row, char* left_header_name) {
    struct header_value* h_v = left_row->header_row->first_val;
    while (h_v) {
        if (strcmp(h_v->name, left_header_name)!=0){
            switch (h_v->type) {
                case TYPE_INT32:
                    print_int(left_row->values, h_v->offset_of_value);
                    break;
                case TYPE_STRING:
                    print_string(left_row->values, h_v->offset_of_value);
                    break; 
                case TYPE_FLOAT:
                    print_float(left_row->values, h_v->offset_of_value);
                    break; 
                case TYPE_BOOL:
                    print_bool(left_row->values, h_v->offset_of_value);
                    break;
            }
        }
        h_v = h_v->next_header_value;
    }
    print_row(right_row);
}


void print_message(char* message) {
    printf("%s | ", message);
}

void print_row_header (struct row* row) {
    struct header_value* h_v = row->header_row->first_val;
    while (h_v) {
        print_message(h_v->name);
        h_v = h_v->next_header_value;
    }
    printf("\n");
}

void print_connected_rows_header (struct row* left_row, struct row* right_row, char* left_header_name) {
    struct header_value* h_v = left_row->header_row->first_val;
    while (h_v) {
        if (strcmp(h_v->name, left_header_name)!=0){
            print_message(h_v->name);
        }
        h_v = h_v->next_header_value;
    }
    print_row_header(right_row);
}
