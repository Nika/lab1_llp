#include "../include/query/query.h"

struct query* get_query(struct table* cur_table, char* column_name, void* value) {
    struct query* query = (struct query*) malloc(sizeof(struct query));
    if (!query) {
        print_error("Can't create new query", "");
        return NULL;
    }
    query->table = cur_table;
	strcpy(query->header_name, column_name);
	query->old_value = value;
	query->new_value = NULL;
    return query;
}

struct query* get_select_query(struct table* cur_table, char* column_name, void* value) {
    struct query* select_query = get_query(cur_table, column_name, value);
    if (!select_query) {
        return NULL;
    }
    select_query->type = QUERY_SELECT;
    return select_query;
}

struct query* get_update_query(struct table* cur_table, char* column_name, void* old_value, void* new_value) {
    struct query* update_query = get_query(cur_table, column_name, old_value);
    if (!update_query) {
        return NULL;
    }
    update_query->type = QUERY_UPDATE;
	update_query->new_value = new_value;
    return update_query;
}

struct query* get_delete_query(struct table* cur_table, char* column_name, void* value) {
    struct query* delete_query = get_query(cur_table, column_name, value);
    if (!delete_query) {
        return NULL;
    }
    delete_query->type = QUERY_DELETE;
    return delete_query;
}

struct join_query* get_join_query(struct table* left_table, struct table* right_table, char* left_header_name,  char* right_header_name){
    struct join_query* query = (struct join_query*) malloc(sizeof(struct join_query));
    if (!query) {
        print_error("Can't create new join query", "");
        return NULL;
    }
    query->left_table = left_table;
    query->right_table = right_table;
    strcpy(query->left_header_name, left_header_name);
    strcpy(query->right_header_name, right_header_name);
    return query;
}

enum query_result execute_delete_query(struct query* cur_query) { 
    print_info("Execute delete query for table %s", cur_query->table->schema->name);
    int cur_row_number = 0;
    struct page_header* cur_page = get_page(cur_query->table->start_page_number);
    if (!cur_page) {
        return QUERY_EXECUTE_ERROR;
    }
    struct row* cur_row = create_row(cur_query->table->schema->header);
    bool is_first_page = true;
    int rows_count = cur_query->table->rows_count;
    while (cur_row_number < rows_count) {
        if (cur_row_number < count_rows_in_page(cur_page, cur_row, is_first_page)){
            read_smth(cur_row->values, cur_page, get_row_offset(cur_row, cur_row_number, is_first_page), cur_row->header_row->size);
            bool result = compare_value_with_field_in_row(cur_row, cur_query->old_value, cur_query->header_name);
            if (result) {
                delete_row_from_page(cur_page, cur_row, cur_row_number, is_first_page);
                add_to_table_stack(cur_query->table, cur_page->page_number);
                cur_query->table->rows_count -= 1;
                rows_count -= 1;
                cur_row_number--;
            }
            cur_row_number++;
        } else {
            is_first_page = false;
            uint32_t next_page_number = cur_page->next_page_number;
            cur_row_number = 0;
            rows_count -= count_rows_in_page(cur_page, cur_row, is_first_page);
            unmap_smth(cur_page);
            cur_page = get_page(next_page_number);
            if (!cur_page) {
                return QUERY_EXECUTE_ERROR;
            }
        }
    }
    unmap_smth(cur_page);
    return QUERY_EXECUTE_OK; 
}


enum query_result execute_update_query(struct query* cur_query) { 
    print_info("Execute update query for table %s", cur_query->table->schema->name);
    uint32_t cur_row_number = 0;
    struct page_header* cur_page = get_page(cur_query->table->start_page_number);
    if (!cur_page) {
        return QUERY_EXECUTE_ERROR;
    }
    struct row* cur_row = create_row(cur_query->table->schema->header);
    uint32_t rows_count = cur_query->table->rows_count;
    bool is_first_page = true;
    while (cur_row_number < rows_count) {
        if (cur_row_number < count_rows_in_page(cur_page, cur_row, is_first_page)){
            read_smth(cur_row->values, cur_page, get_row_offset(cur_row, cur_row_number, is_first_page), cur_row->header_row->size);
            bool result = compare_value_with_field_in_row(cur_row, cur_query->old_value, cur_query->header_name);
            if (result) {
                update_row_in_page(cur_page, cur_row, cur_row_number, is_first_page, cur_query->new_value, cur_query->header_name);
            }
            cur_row_number++;
        } else {
            is_first_page = false;
            cur_row_number = 0;
            rows_count -= count_rows_in_page(cur_page, cur_row, is_first_page);
            uint32_t next_page_number = cur_page->next_page_number;
            unmap_smth(cur_page);
            cur_page = get_page(next_page_number);
            if (!cur_page) {
                return QUERY_EXECUTE_ERROR;
            }
        }
    }
    unmap_smth(cur_page);
    return QUERY_EXECUTE_OK; 
}

enum query_result execute_select_query(struct query* cur_query) { 
    print_info("Execute select query for table %s", cur_query->table->schema->name);
    uint32_t cur_row_number = 0;
    struct page_header* cur_page = get_page(cur_query->table->start_page_number);
    if (!cur_page) {
        return QUERY_EXECUTE_ERROR;
    }
    struct row* cur_row = create_row(cur_query->table->schema->header);
    bool is_first_page = true;
    uint32_t rows_count = cur_query->table->rows_count;
    print_row_header(cur_row);
    while (cur_row_number < rows_count) {
        if (cur_row_number < count_rows_in_page(cur_page, cur_row, is_first_page)){
            read_smth(cur_row->values, cur_page, get_row_offset(cur_row, cur_row_number, is_first_page), cur_row->header_row->size);
            bool result = compare_value_with_field_in_row(cur_row, cur_query->old_value, cur_query->header_name);
            if (result) {
                print_row(cur_row);
            }
            cur_row_number++;
        } else {
            is_first_page = false;
            cur_row_number = 0;
            rows_count -= count_rows_in_page(cur_page, cur_row, is_first_page);
            uint32_t next_page_number = cur_page->next_page_number;
            unmap_smth(cur_page);
            cur_page = get_page(next_page_number);
            if (!cur_page) {
                return QUERY_EXECUTE_ERROR;
            }
        }
    }
    unmap_smth(cur_page);
    return QUERY_EXECUTE_OK; 
}


enum query_result execute_join_query(struct join_query* cur_query) { 
    print_info("Execute join query for tables", "");
    //select for left table
    int cur_row_number = 0;
    struct page_header* cur_page = get_page(cur_query->left_table->start_page_number);
    struct row* cur_row = create_row(cur_query->left_table->schema->header);
    struct row* cur_row_r = create_row(cur_query->right_table->schema->header);
    bool is_first_page = true;
    int rows_count = cur_query->left_table->rows_count;
    print_connected_rows_header(cur_row, cur_row_r, cur_query->left_header_name);
    while (cur_row_number < rows_count) {
        if (cur_row_number < count_rows_in_page(cur_page, cur_row, is_first_page)){
            read_smth(cur_row->values, cur_page, get_row_offset(cur_row, cur_row_number, is_first_page), cur_row->header_row->size);
            // select for right table
            int cur_row_number_r = 0;
            struct page_header* cur_page_r = get_page(cur_query->right_table->start_page_number);
            bool is_first_page_r = true;
            int rows_count_r = cur_query->right_table->rows_count;
            while (cur_row_number_r < rows_count_r) {
                if (cur_row_number_r < count_rows_in_page(cur_page_r, cur_row_r, is_first_page_r)){
                    read_smth(cur_row_r->values, cur_page_r, get_row_offset(cur_row_r, cur_row_number_r, is_first_page_r), cur_row_r->header_row->size);

                    bool result = compare_fields_in_rows(cur_row, cur_row_r, cur_query->left_header_name, cur_query->right_header_name);
                    if (result) {
                        print_connected_rows(cur_row, cur_row_r, cur_query->left_header_name);
                    }
                    cur_row_number_r++;
                } else {
                    is_first_page_r = false;
                    cur_row_number_r = 0;
                    rows_count_r -= count_rows_in_page(cur_page_r, cur_row_r, is_first_page_r);
                    uint32_t next_page_number_r = cur_page_r->next_page_number;
                    unmap_smth(cur_page_r);
                    cur_page_r = get_page(next_page_number_r);
                }
            }
            unmap_smth(cur_page_r);
            cur_row_number++;
        } else {
            is_first_page = false;
            cur_row_number = 0;
            rows_count -= count_rows_in_page(cur_page, cur_row, is_first_page);
            uint32_t next_page_number = cur_page->next_page_number;
            unmap_smth(cur_page);
            cur_page = get_page(next_page_number);
        }
    }
    unmap_smth(cur_page);
    return QUERY_EXECUTE_OK; 
}


enum query_result execute_query(struct query* cur_query) {
    switch (cur_query->type) {
		case QUERY_DELETE:
			return execute_delete_query(cur_query);
		case QUERY_UPDATE:
			return execute_update_query(cur_query);
		case QUERY_SELECT:
			return execute_select_query(cur_query);
	}
}

void free_query(struct query* old_query) {
    free(old_query);
}

void free_join_query(struct join_query* old_query) {
    free(old_query);
}