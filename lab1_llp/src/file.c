#include "../include/file/file.h"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#if defined(__linux__) || defined(__APPLE__)

#include <sys/mman.h>

int fd;

enum file_status create_and_open_file(char* name){
	print_info("Create and open file %s", name);
	fd = open(name, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	if (fd == -1){
		return OPEN_ERROR;
	}
	return OPEN_OK;
}

enum file_status open_file(char* name){
	print_info("Open file %s", name);
	fd = open(name, O_RDWR);
	if (fd == -1){
		return OPEN_ERROR;
	}
	return OPEN_OK;
}

int close_file() {
	print_info("Close file", "");
	return close(fd);
}

uint64_t get_file_size() {
	struct stat _fileStatbuff;
	if (fd == -1) {
		return -1;
	} else {
		if ((fstat(fd, &_fileStatbuff) != 0) || (!S_ISREG(_fileStatbuff.st_mode))) {
			return -1;
		} else {
			return _fileStatbuff.st_size;
		}
	}
}

uint32_t get_last_page_number() {
	return get_file_size(fd)/MAX_PAGE_AMOUNT;
}


void* mmap_page(int page_number) {
	void* cur_addr = mmap(NULL, MAX_PAGE_AMOUNT, PROT_READ | PROT_WRITE,
		MAP_SHARED, fd, (page_number - 1)*MAX_PAGE_AMOUNT);
	if (cur_addr == MAP_FAILED) {
		return NULL;
	}
	return cur_addr;
}

void* mmap_new_page(int page_number) {
	print_info("Map new page number %d", page_number);
	lseek(fd, MAX_PAGE_AMOUNT, SEEK_END);
	write(fd, "", 1);
	lseek(fd, 0, SEEK_END);
	return mmap_page(page_number);
}

void unmap_smth(void* addr) {
	if (addr) {
		munmap(addr, MAX_PAGE_AMOUNT);
	}
}

#endif

#if defined(_WIN32)

#include <windows.h>

HANDLE fd;

enum file_status create_and_open_file(char* name){
	print_info("Create and open file %s", name);
	fd = CreateFile(name, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if (fd == INVALID_HANDLE_VALUE) {
        return OPEN_ERROR;
    }

	return OPEN_OK;
}

enum file_status  open_file(char* name){
	print_info("Open file %s", name);
	fd = CreateFile(name, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if (fd == INVALID_HANDLE_VALUE) {
        return OPEN_ERROR;
    }

	return OPEN_OK;
}

int close_file() {
	print_info("Close file", "");
	return CloseHandle(fd);
}

uint64_t get_file_size() {
	return (uint64_t) GetFileSize(fd, NULL);
}

uint32_t get_last_page_number() {
	return get_file_size(fd)/MAX_PAGE_AMOUNT;
}

void* mmap_page(int page_number) {
	HANDLE hMapFile = CreateFileMapping(fd, NULL, PAGE_READWRITE, 0, 0, NULL);
    if (hMapFile == NULL) {
        return NULL;
    }
    return MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0,  (page_number - 1)*MAX_PAGE_AMOUNT), MAX_PAGE_AMOUNT);
}

void* mmap_new_page(int page_number) {
	print_info("Map new page number %d", page_number);
	SetFilePointer(fd, MAX_PAGE_AMOUNT, NULL, FILE_END);
	SetEndOfFile(fd);
	return mmap_page(page_number);
}

void unmap_smth(void* addr) {
	if (addr) {
		FlushFileBuffers(fd);
		UnmapViewOfFile(addr);
	}
}

#endif

void read_smth(void* element, struct page_header* cur_page, uint32_t offset, uint32_t size) {
	memcpy(element, ((char*)cur_page) + offset, size);
}

enum write_status write_smth(struct page_header* cur_page, void* element, size_t size) {
	if (cur_page->free_size < size) {
	 	return WRITE_ERROR;
	}
	memcpy(((char*)cur_page) + cur_page->current_place, element, size);
	cur_page->current_place = cur_page->current_place + size;
	cur_page->free_size = cur_page->free_size - size;
	return WRITE_OK;
}

int is_enough_place_in_meta_page(struct meta_page* page) {
	return page->count_tables < sizeof(page->tables)/sizeof(struct table_info);
}

struct meta_page* init_meta_page(uint32_t number) {
	struct meta_page* new_page = (struct meta_page*) mmap_new_page(number);
	new_page->page_number = number;
	new_page->count_tables = 0;
	return new_page;
}

struct meta_page* get_meta_page(uint32_t number) {
	struct meta_page* new_page = (struct meta_page*) mmap_page(number);
	return new_page;
}

struct meta_page* get_all_meta_pages() {
	print_info("Read all meta pages", "");
	struct meta_page* new_page = (struct meta_page*) mmap_page(1);
	struct meta_page* cur_page = new_page;
	while (cur_page->next_page_number != 0) {
		cur_page->next_meta_page = (struct meta_page*) mmap_page(cur_page->next_page_number);
		cur_page = cur_page->next_meta_page;
	}
	return new_page;
}

struct page_header* get_page(uint32_t number) {
	struct page_header* page_header = (struct page_header*) mmap_page(number);
    return page_header;
}

struct page_header* init_page(uint32_t number, bool is_new) {
	struct page_header* page_header = NULL;
	if (is_new == true) {
		page_header = (struct page_header*) mmap_new_page(number);
	} else {
		page_header = (struct page_header*) mmap_page(number);
	}
	if (!page_header) return NULL;
	page_header->page_number = number;
    page_header->is_empty = 1;
    page_header->size = MAX_PAGE_AMOUNT;
    page_header->free_size = MAX_PAGE_AMOUNT - sizeof(struct page_header);
    page_header->current_place = sizeof(struct page_header);
    page_header->next_page_number = 0;
    return page_header;
}

int32_t clear_page(uint32_t number) {
	struct page_header* page_header = (struct page_header*) mmap_page(number);
	if (!page_header) return -1;
    page_header->is_empty = 1;
    page_header->size = MAX_PAGE_AMOUNT;
    page_header->free_size = MAX_PAGE_AMOUNT - sizeof(struct page_header);
    page_header->current_place = sizeof(struct page_header);
	uint32_t connected_page = page_header->next_page_number;
    page_header->next_page_number = 0;
	unmap_smth(page_header);
    return connected_page;
}

enum write_status save_new_table_in_file(struct meta_page* meta_page, char* name) {
	struct meta_page* cur_page = meta_page;
	int is_saved = 0;
	while (!is_saved) {
		if (is_enough_place_in_meta_page(cur_page)) {
			struct table_info* table_info = (struct table_info*) malloc(sizeof(struct table_info));
			strcpy(table_info->name, name);
			table_info->start_page_number = get_last_page_number() + 1;
			table_info->end_page_number = table_info->start_page_number;
			table_info->rows_count = 0;
			cur_page->tables[cur_page->count_tables] = *table_info;
			cur_page->count_tables = cur_page->count_tables + 1;
			struct page_header* new_page = init_page(table_info->start_page_number, true);
			if (!new_page) {
				print_error("Can't init new page number %d", table_info->start_page_number);
				return WRITE_ERROR;
			}
			unmap_smth(new_page);
			is_saved = 1;
		} else {
			if (!cur_page->next_page_number) {
				int cur_page_number = get_last_page_number() + 1;
				print_info("Add new meta page number %d", cur_page_number);
				cur_page->next_meta_page = init_meta_page(cur_page_number);
				cur_page->next_page_number = cur_page_number;
			}
			cur_page = cur_page->next_meta_page;
		}
	}
	return WRITE_OK;
}

void unmap_all_meta_pages(struct meta_page* meta_page){
	print_info("Unmap all meta pages", "");
	struct meta_page* prev_page = meta_page;
	struct meta_page* cur_page = meta_page;
	while (cur_page->next_page_number != 0) {
		prev_page = cur_page;
		cur_page = cur_page->next_meta_page;
		unmap_smth(prev_page);
	}
	unmap_smth(cur_page);
}	