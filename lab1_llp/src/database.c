#include "../include/db/database.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void free_db(struct database* old_db){
	unmap_all_meta_pages(old_db->meta_page);
	close_file();
	free(old_db);
}

void rewrite_table_info(struct table* table) {
	struct meta_page* cur_mp = table->db->meta_page;
	char* name = table->schema->name;
	strcpy(name, table->schema->name);
	int flag = 0;
	while (cur_mp != NULL && !flag) {
		for (int i = 0; i < cur_mp->count_tables; i++) {
			if (strcmp(cur_mp->tables[i].name, name) == 0) {
				flag = 1;
				cur_mp->tables[i].start_page_number = table->start_page_number;
				cur_mp->tables[i].end_page_number = table->end_page_number;
				cur_mp->tables[i].rows_count = table->rows_count;
				break;
			}
		}
		cur_mp = cur_mp->next_meta_page;
	}
	if (flag) {
		//print_info("Rewrite table info %s", table->schema->name);
	}
	
}

int check_table_name(char* name, struct database* db) {
	struct meta_page* cur_mp = db->meta_page;
	int flag = 1;
	while (cur_mp != NULL && flag) {
		for (int i = 0; i < cur_mp->count_tables; i++) {
			if (strcmp(cur_mp->tables[i].name, name) == 0) {
				
				flag = 0;
			}
		}
		cur_mp = cur_mp->next_meta_page;
	}
	return flag;
}

int delete_table_from_meta_pages(char* name, struct database* db) {
	struct meta_page* cur_mp = db->meta_page;
	int flag = 0;
	while (cur_mp != NULL && !flag) {
		for (int i = 0; i < cur_mp->count_tables; i++) {
			if (strcmp(cur_mp->tables[i].name, name) == 0) {
				flag = 1;
				if (i < cur_mp->count_tables - 1) {
					memcpy(&(cur_mp->tables[i]), &(cur_mp->tables[i+1]), (cur_mp->count_tables - i - 1)*sizeof(struct table_info));
				}
				cur_mp->count_tables -= 1;
				break;
			}
		}
		cur_mp = cur_mp->next_meta_page;
	}
	
	return flag;
}

void add_to_db_stack(struct database* db, uint64_t page_number) {
	struct free_page_stack* new_stack_el = (struct free_page_stack*) malloc(sizeof(struct free_page_stack));
	new_stack_el->prev_el = db->stack;
	new_stack_el->page_number = page_number;
	db->stack = new_stack_el;
}

uint64_t take_from_db_stack(struct database* db) {
	if (db->stack == NULL) {
		return -1;
	}
	struct free_page_stack* cur_el = db->stack;
	db->stack = db->stack->prev_el;
	uint64_t page_number = cur_el->page_number;
	free(cur_el);
	return page_number;
}

struct database* create_db_in_file(char* name) {
	if (create_and_open_file(name) == OPEN_ERROR) {
		print_error("Can't open file %s", name);
		return NULL;
	}
	struct database* db = (struct database*) malloc(sizeof(struct database));
	db->stack = NULL;
	db->meta_page = init_meta_page(1);
	if (db->meta_page == NULL) {
		free_db(db);
		print_error("Can't create new database in file %s", name);
		return NULL;
	}
	print_info("Database created in file %s", name);
	return db;
}

struct database* get_db_from_file(char* name) {
	if (open_file(name) == OPEN_ERROR) {
		print_error("Can't open file %s", name);
		return NULL;
	}
	struct database* db = (struct database*) malloc(sizeof(struct database));
	db->stack = NULL;
	db->meta_page = get_all_meta_pages();
	if (db->meta_page == NULL){
		print_error("Can't get database from file %s", name);
		return NULL;
	}
	print_info("Database read from file %s", name);
	return db;
}

struct table* get_table_from_db(char* name, struct database* db) {
	if (check_table_name(name, db)) {
		print_error("Table %s not found", name);
		return NULL;
	}
	struct table_info* t_info = NULL;
	struct meta_page* cur_mp = db->meta_page;
	while (cur_mp != NULL && !t_info) {
		for (int i = 0; i < cur_mp->count_tables; i++) {
			if (strcmp(cur_mp->tables[i].name, name) == 0) {
				t_info = &(cur_mp->tables[i]);
			}
		}
		cur_mp = cur_mp->next_meta_page;
	}
	struct table* old_table = (struct table*) malloc(sizeof(struct table));
	old_table->start_page_number=t_info->start_page_number;
	old_table->end_page_number=t_info->end_page_number;
	old_table->rows_count=t_info->rows_count;
	struct page_header* cur_page = get_page(old_table->start_page_number);
	struct schema* schema = (struct schema*) malloc(sizeof(struct schema));
	strcpy(schema -> name, name);
	old_table->schema = schema;
	old_table->db = db;
	schema->header = (struct header_row*) malloc(sizeof(struct header_row));
	schema->header->first_val = (struct header_value*) malloc(sizeof(struct header_value));
	struct header_value* h_v = schema->header->first_val;
	read_smth(h_v, cur_page, sizeof(struct page_header), sizeof(struct header_value));
	uint32_t offset = sizeof(struct page_header);
	uint32_t size = h_v->size;
	uint32_t size_of_header = sizeof(struct header_value);
	while (h_v -> has_next){
		offset += sizeof(struct header_value);
		h_v->next_header_value = (struct header_value*) malloc(sizeof(struct header_value));
		h_v = h_v->next_header_value;
		read_smth(h_v, cur_page, offset, sizeof(struct header_value));
		size = size + h_v->size;
		size_of_header += sizeof(struct header_value);
	}
	schema->header->size = size;
	schema->header->size_of_header = size_of_header;
	print_info("Table %s read ", name);
	return old_table;
}

enum add_status add_table_to_db(struct table* new_table, struct database* db) {
	new_table->db = db;
	if (!check_table_name(new_table->schema->name, db)) {
		print_error("Table %s is already exist ", new_table->schema->name);
		return ADD_ERROR_DUPLICATE;
	}
	enum write_status ws = save_new_table_in_file(db->meta_page, new_table->schema->name);
	if (ws == WRITE_ERROR) {
		print_error("Table %s can't been added to db", new_table->schema->name);
		return ADD_ERROR;
	}
	struct meta_page* cur_mp = db->meta_page;
	while (cur_mp->next_meta_page) {
		cur_mp = cur_mp->next_meta_page;
	}
	new_table->start_page_number=cur_mp->tables[cur_mp->count_tables-1].start_page_number;
	new_table->end_page_number=cur_mp->tables[cur_mp->count_tables-1].end_page_number;
	struct page_header* cur_page = init_page(new_table->start_page_number, false);
	struct header_row* row_h = new_table->schema->header;
	struct header_value* h_v = row_h->first_val;
	while (h_v) {
		enum write_status ws = write_smth(cur_page, h_v, sizeof(struct header_value));
		while (ws == WRITE_ERROR) {
			if (cur_page->next_page_number){
				uint32_t next_pn = cur_page->next_page_number;
				unmap_smth(cur_page);
				cur_page = init_page(next_pn, false);
			} else { 
				int new_page_number = get_last_page_number() + 1;
				struct page_header* new_page = init_page(new_page_number, true);
				cur_page->next_page_number = new_page_number;
				unmap_smth(cur_page);
				cur_page = new_page;
			}
			enum write_status ws = write_smth(cur_page, h_v, sizeof(struct header_value));
		}
		h_v = h_v -> next_header_value;
	}
	print_info("Table %s schema add to db ", new_table->schema->name);
	return ADD_OK;
}
