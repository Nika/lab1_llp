#pragma once
#include "../db/table.h"
#include "output_util.h"
#include <stdlib.h>
#include <string.h>

enum query_type {
	QUERY_SELECT,
	QUERY_UPDATE,
	QUERY_DELETE
};

enum query_result {
	QUERY_EXECUTE_OK,
	QUERY_EXECUTE_ERROR
};

struct query {
	enum query_type type;
	struct table* table;
	char header_name[NAME_MAX];
	void* old_value;
	void* new_value;
};

struct join_query {
	struct table* left_table;
	struct table* right_table;
	char left_header_name[NAME_MAX];
	char right_header_name[NAME_MAX];
};


struct query* get_select_query(struct table* cur_table, char* column_name, void* value);

struct query* get_update_query(struct table* cur_table, char* column_name, void* old_value, void* new_value);

struct query* get_delete_query(struct table* cur_table, char* column_name, void* value);

struct join_query* get_join_query(struct table* left_table, struct table* right_table, char* left_header_name,  char* right_header_name);

enum query_result execute_join_query(struct join_query* cur_query);

enum query_result execute_query(struct query* cur_query);

void free_query(struct query* old_query);

void free_join_query(struct join_query* old_query);