#include "../db/row.h"
#include "../db/table.h"

void print_row (struct row* row);

void print_row_header (struct row* row);

void print_message(char* message);

void print_row_header_from_schema (struct schema* schema);

void print_connected_rows(struct row* left_row, struct row* right_row, char* left_header_name);

void print_connected_rows_header (struct row* left_row, struct row* right_row, char* left_header_name);