#pragma once

#define VALUE_NAME_MAX   40
#include "../file/file.h"
#include <inttypes.h>
#include <stdbool.h>

enum value_type {
	TYPE_INT32 = 0,
	TYPE_STRING,
	TYPE_FLOAT,
	TYPE_BOOL
};

struct header_row {
	struct header_value* first_val;
	uint32_t size;  //size of one row
	uint32_t size_of_header;
};

struct row {
	struct header_row* header_row;
	struct row* next_row;
	struct row* prev_row;
	void* values;
};

struct header_value {
	enum value_type type;
	char name [VALUE_NAME_MAX];
	bool has_next;
	struct header_value* next_header_value;
	uint32_t size;
	uint32_t offset_of_value;
};


void free_header_row(struct header_row* old_row);

void free_row(struct row* old_row);

struct row* create_row(struct header_row* header);

uint32_t get_row_offset(struct row* cur_row, uint32_t cur_row_number, bool is_first_page);

void delete_row_from_page(struct page_header* cur_page, struct row* cur_row, uint32_t cur_row_number, bool is_first_page);

void update_row_in_page(struct page_header* cur_page, struct row* cur_row, uint32_t cur_row_number, bool is_first_page, void* new_value,  char* column_name);


uint32_t count_rows_in_page(struct page_header* page, struct row* row, bool is_first_page);

struct header_value* get_header_value_of_col(struct header_row* header, char* col_name);

int insert_value_in_row(struct row* row, char* col_name, void* value);

bool compare_value_with_field_in_row(struct row* row, void* value, char* column_name);

bool compare_fields_in_rows(struct row* left_row, struct row* right_row, char* left_header_name, char* right_header_name);

struct header_row* add_column_to_table_header(struct header_row* header, struct header_value* header_value, enum value_type type, char* name);
