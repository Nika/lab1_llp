#pragma once

#include "./row.h"
#include "./database.h"

struct schema {
	char name[NAME_MAX];
	struct header_row* header;
};

struct table {
	uint32_t start_page_number;
	uint32_t end_page_number;
	uint32_t rows_count;
	struct database* db;
	struct schema* schema;
	struct free_page_stack* stack;
};

void add_to_table_stack(struct table* table, uint64_t page_number);

uint64_t take_from_table_stack(struct table* table);

struct row* create_row_in_table(struct table* table);

struct table* create_table_from_schema(struct schema* schema, struct database* db);

enum write_status insert_row_in_table(struct row* row, struct table *new_table);

struct schema* create_new_schema(char* t_name);

struct table* get_table_from_file (char* t_name, struct database* db);
 
struct schema* add_simple_column_to_table_header(struct schema* schema, enum value_type type, char* name);

struct schema* add_string_column_to_table_header(struct schema* schema, enum value_type type, char* name, uint32_t len);

void delete_table(struct table* old_table);

void free_table(struct table* old_table);
