#pragma once

#include "table.h"

enum add_status{
	ADD_OK,
	ADD_ERROR_DUPLICATE,
	ADD_ERROR
};

struct database {
	struct free_page_stack* stack;
	struct meta_page* meta_page;
};

void add_to_db_stack(struct database* db, uint64_t page_number);

uint64_t take_from_db_stack(struct database* db);

void free_db(struct database* old_db);

void rewrite_table_info(struct table* table);

struct database* get_db_from_file(char* name);

struct database* create_db_in_file(char* name);

enum add_status add_table_to_db(struct table* new_table, struct database* db);

struct table* get_table_from_db(char* name, struct database* db);

int delete_table_from_meta_pages(char* name, struct database* db);
