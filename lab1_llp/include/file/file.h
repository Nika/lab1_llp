#pragma once
#include "./page.h"
#include <stddef.h>
#include <stdbool.h>

enum file_status {
	OPEN_OK,
	OPEN_ERROR
};

enum write_status {
	WRITE_OK,
	WRITE_ERROR
};

enum read_status {
	READ_OK,
	READ_ERROR
};

enum mmap_status {
	MMAP_OK,
	MMAP_ERROR
};

enum file_status open_file(char* name);

enum file_status create_and_open_file(char* name);

int close_file();

uint64_t get_file_size();

void unmap_all_meta_pages(struct meta_page* meta_page);

void* mmap_page(int page_number);

void* mmap_new_page(int page_number);

void unmap_smth(void* addr);

void read_smth(void* element, struct page_header* cur_page, uint32_t offset, uint32_t size);

enum write_status write_smth(struct page_header* cur_page, void* element, size_t size);

uint32_t get_last_page_number();

struct meta_page* init_meta_page(uint32_t number);

struct meta_page* get_meta_page(uint32_t number);

struct meta_page* get_all_meta_pages();

struct page_header* get_page(uint32_t number);

struct page_header* init_page(uint32_t number, bool is_new);

int32_t clear_page(uint32_t number);

enum write_status save_new_table_in_file(struct meta_page* meta_page, char* name);


