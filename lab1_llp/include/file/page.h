#pragma once
#include "../logger.h"
#include <inttypes.h>
#define MAX_PAGE_AMOUNT 4096
#define NAME_MAX   40
#define MAX_TABLE_COUNT MAX_PAGE_AMOUNT/sizeof(struct table_info)

struct free_page_stack {
    uint32_t page_number;
    struct free_page_stack* prev_el;
};


struct table_info{
    char name[NAME_MAX];
    uint32_t start_page_number;
    uint32_t end_page_number;
    uint32_t rows_count;
};

struct meta_page {
    uint32_t page_number;
    uint32_t next_page_number;
    struct meta_page* next_meta_page;
    int count_tables;
    struct table_info tables[MAX_TABLE_COUNT];
};

struct page_header {
    uint32_t page_number;
    int is_empty;
    uint32_t size;
    uint32_t free_size;
    uint32_t current_place;
    uint32_t next_page_number;
};
